#if !defined(_RHEL_IMAGE)
#define _RHEL_IMAGE ubi
#endif

#if !defined(_RHEL_IMAGE_TAG) && defined(_RHEL_MINOR_VERSION)
#define _RHEL_IMAGE_TAG _RHEL_VERSION._RHEL_MINOR_VERSION
#elif !defined(_RHEL_IMAGE_TAG)
#define _RHEL_IMAGE_TAG latest
#endif

FROM registry.access.redhat.com/_RHEL_IMAGE/**/_RHEL_VERSION/_RHEL_IMAGE:_RHEL_IMAGE_TAG
LABEL maintainer="CKI Project Team <cki-project@redhat.com>"
LABEL name=_IMAGE_NAME

/* Make sure that if a | fails, the build fails */
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

#ifdef _USE_YUM_PKG_MANAGER
ARG RPM_PKG_MANAGER_SETUP=yum
#else
ARG RPM_PKG_MANAGER_SETUP=dnf
#endif

/* Gitlab API Base URL */
ARG CI_API_V4_URL=https://gitlab.com/api/v4
/* Backports project id (https://gitlab.com/cki-project/rhel-backports) */
ARG BACKPORTS_PROJECT_ID=36546257

#include "snippet-envvars"
#include "snippet-certs"
#include "snippet-cki-home"
#include "snippet-curlrc"

#ifdef _USE_YUM_PKG_MANAGER
/* Make sure yum fails when packages are not available */
RUN echo "skip_missing_names_on_install=False" >> /etc/yum.conf
#else
/* Allow not only best candidate packages */
RUN sed -i "/^best/d;\$abest=False" /etc/dnf/dnf.conf
#endif

/* Add the internal RHEL repositories. */
RUN rm -fv "/etc/yum.repos.d/"*
#ifdef _RHEL_MINOR_VERSION
COPY files/rhel/**/_RHEL_VERSION/**/.z.repo /etc/yum.repos.d/
#ifdef _ZSTREAM_REPO_NAME
 # hadolint ignore=SC2026
RUN sed -i 's|__Z__|'_ZSTREAM_REPO_NAME'|g' /etc/yum.repos.d/rhel/**/_RHEL_VERSION/**/.z.repo
#else
RUN sed -i 's|__Z__|'._RHEL_MINOR_VERSION'|g' /etc/yum.repos.d/rhel/**/_RHEL_VERSION/**/.z.repo
#endif
#if _RHEL_VERSION == 7
 # hadolint ignore=SC2026
RUN sed -i 's|__SUPPORT__|'_SUPPORT_LEVEL'|g' /etc/yum.repos.d/rhel/**/_RHEL_VERSION/**/.z.repo
#endif
#else
COPY files/rhel/**/_RHEL_VERSION/**/.repo /etc/yum.repos.d/
#endif

/* Disable the subscription-manager plugin to remove the warning */
RUN sed -i 's|enabled=1|enabled=0|g' /etc/yum/pluginconf.d/subscription-manager.conf

/* Add ts from moreutils for timestamping long running tasks. */
COPY files/ts /usr/bin/

#if _RHEL_VERSION == 6
RUN $RPM_PKG_MANAGER_SETUP -y install \
      "https://archives.fedoraproject.org/pub/archive/epel/6/x86_64/epel-release-6-8.noarch.rpm"
#elif _RHEL_VERSION == 7
RUN if [ "$(arch)" = "s390x" ] ; then \
      $RPM_PKG_MANAGER_SETUP -y install \
        "http://download.sinenomine.net/clefos/epel7/noarch/epel-release-7-13.noarch.rpm" ; \
      sed -i 's|gpgcheck=1|gpgcheck=0|g' /etc/yum.repos.d/epel*.repo ; \
    else \
      $RPM_PKG_MANAGER_SETUP -y install \
        https://dl.fedoraproject.org/pub/epel/epel-release-latest-_RHEL_VERSION.noarch.rpm ; \
    fi
#else
/* Don't put quotes to the URL, it'd break the cpp variable conversion */
RUN $RPM_PKG_MANAGER_SETUP -y install \
      https://dl.fedoraproject.org/pub/epel/epel-release-latest-_RHEL_VERSION.noarch.rpm

#if (_RHEL_VERSION == 8)
/* EPEL cross compilers are too new for RHEL8 kernels */
RUN sed -i '/^\[epel\]/,/^\[/s/^$/exclude=binutils-*-linux-gnu gcc-*-linux-gnu glibc-*-linux-gnu\n/' /etc/yum.repos.d/epel.repo
#endif
#endif

/* Upgrade all packages */
RUN $RPM_PKG_MANAGER_SETUP -y upgrade
